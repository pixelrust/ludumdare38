package org.ludumdare38

import com.badlogic.gdx.graphics.Color
import org.bagofgameutils.Game
import org.bagofgameutils.gdxutils.FontInfo
import org.bagofgameutils.layer.layers.CenteredTextLayer
import org.bagofgameutils.quitprocessor.QuitProcessor
import org.bagofutils.entitysystem.World


/**
 * LudumDare38 Game Compo entry.
 */
class MainGame: Game("LudumDare38") {

    override fun createProcessors(world: World) {
        // Quit from ESC press
        world.addProcessor(QuitProcessor())

        screenClearProcessor.backgroundColor.set(Color.DARK_GRAY)
    }

    override fun setupWorld(world: World) {
        // Test text
        // TODO: Scheduler service to run update code after specified time, or at specified intervals (until stopped)
        world.createEntity(CenteredTextLayer("Game Over", FontInfo("DefaultFont.ttf", 64)))
    }

}