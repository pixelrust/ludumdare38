package org.ludumdare38

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import org.bagofgameutils.gdxutils.buildTextureAtlas

fun main(args: Array<String>) {

    // Build texture atlases
    buildTextureAtlas("asset_sources/tiles/", "src/main/resources/tiles", "tiles.atlas")



    val config = LwjglApplicationConfiguration()
    LwjglApplication(MainGame(), config)
}
